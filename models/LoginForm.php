<?php


namespace app\models;


use app\core\Application;
use app\core\DatabaseModel;
use app\core\Model;
use app\core\UserModel;

class LoginForm extends User
{
    public string $email = '';
    public string $password = '';


    public function rules(): array
    {
        return [
            'email' => [self::REQUIRED, self::EMAIL],
            'password' => [self::REQUIRED],
        ];
    }

    public static function tableName(): string
    {
        return 'users';
    }

    public function attributes(): array
    {
        return ['email', 'password'];
    }


    public function login()
    {
        $user = User::findOne(['email' => $this->email]);
        if (!$user) {
            $this->addError('email', 'User does not exist email address');
            return false;
        }

        if (!password_verify($this->password, $user->password)) {
            $this->addError('password', 'Password is incorrect');
            return false;
        }

        return Application::$app->login($user);
    }

    public function labels(): array
    {
        return [
            'email' => 'Email',
            'password' => 'Password',
        ];
    }
}
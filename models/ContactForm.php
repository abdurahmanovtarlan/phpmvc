<?php


namespace app\models;


use app\core\Model;

class ContactForm extends Model
{
    public string $subject = '';
    public string $email = '';
    public string $body = '';

    public function rules(): array
    {
        return [
            'subject' => [self::REQUIRED],
            'email' => [self::REQUIRED, self::EMAIL],
            'body' => [self::REQUIRED],
        ];
    }

    public function labels(): array
    {
        return [
            'subject' => "Enter your subject",
            'email' => "Enter your email",
            'body' => "Enter your body",
        ];
    }

    public function send()
    {
        return true;
    }
}
<?php


namespace app\models;

use app\core\Application;
use app\core\DatabaseModel;
use app\core\UserModel;

class User extends UserModel
{

    public string $firstname = '';
    public string $lastname = '';
    public string $email = '';
    public string $password = '';
    public int $status = 0;
    public string $confirmPassword = '';

    public static function tableName(): string
    {
        return 'users';
    }

    public static function primaryKey(): string
    {
        return 'id';
    }

    public function save()
    {
        $this->status = 1;
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        return parent::save();
    }


    public function rules(): array
    {
        return [
            'firstname' => [self::REQUIRED],
            'lastname' => [self::REQUIRED],
            'email' => [self::REQUIRED, self::EMAIL, [self::UNIQUE, 'class' => self::class]],
            'password' => [self::REQUIRED, [self::MIN, 'min' => 8], [self::MAX, 'max' => 24]],
            'confirmPassword' => [self::REQUIRED, [self::MATCH, 'match' => "password"]],
        ];
    }


    public function attributes(): array
    {
        return ['firstname', 'lastname', 'email', 'password', 'status'];
    }

    public function labels(): array
    {
        return [
            'firstname' => "First Name",
            'lastname' => "Last Name",
            'email' => "Email",
            'password' => "Password",
            'confirmPassword' => "Confirm Password",
        ];
    }

    public function getDisplayName(): string
    {
        return $this->firstname . ' ' . $this->lastname;
    }


}
<?php


namespace app\models;


use app\core\Application;
use app\core\database\DatabaseModel;
use app\core\Model;

class Post extends DatabaseModel
{

    public string $user_id = '';
    public string $title = '';
    public string $detail = '';
    public string $image = '';

    public static function tableName(): string
    {
        return 'posts';
    }

    public static function primaryKey(): string
    {
        return 'id';
    }

    public function rules(): array
    {
        return [
            'title' => [self::REQUIRED],
            'image' => [self::REQUIRED],
            'detail' => [self::REQUIRED],
        ];
    }

    public function labels(): array
    {
        return [
            'title' => "Post name",
            'detail' => "Post Detail",
            'image' => "Post image",

        ];
    }


    public function attributes(): array
    {
        return ['user_id', 'title', 'detail', 'image'];
    }

    public function save()
    {
        $this->user_id = Application::$app->user->id;
        $file = json_decode($this->image);
        $filename = $file->name;
        $tempname = $file->tmp_name;
        $folder = 'images/' . $filename;
        if (move_uploaded_file($tempname, $folder)) {
            $this->image = $_SERVER['HTTP_ORIGIN'] . '/images/' . $filename;
        }
        return parent::save();
    }


}
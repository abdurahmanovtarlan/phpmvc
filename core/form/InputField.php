<?php


namespace app\core\form;


use app\core\database\DatabaseModel;
use app\core\Model;

class InputField extends MainField
{

    public const TYPE_TEXT = 'text';
    public const TYPE_PASSWORD = 'password';
    public const TYPE_NUMBER = 'number';
    public const TYPE_EMAIL = 'email';
    public const TYPE_FILE = 'file';


    public string $type; // inputun typelari
    public string $value = ''; // inputun typelari
    public Model $model; //modeli aliriq
    public string $attribute; // modelde olan fieldlarin adini aliriq

    /**
     * Field constructor.
     * @param Model $model
     * @param string $attribute
     */
    public function __construct(Model $model, string $attribute, $value)
    {
        $this->value = $value;
        $this->type = self::TYPE_TEXT;
        parent::__construct($model, $attribute);
    }


    public function password()
    {
        $this->type = self::TYPE_PASSWORD;
        return $this;
    }

    public function number()
    {
        $this->type = self::TYPE_NUMBER;
        return $this;
    }

    public function email()
    {
        $this->type = self::TYPE_EMAIL;
        return $this;
    }

    public function file()
    {
        $this->type = self::TYPE_FILE;
        return $this;
    }


    public function renderInput(): string
    {
        return sprintf('<input type="%s" name="%s" value="%s" class="form-control %s">'
            ,
            $this->type,
            $this->attribute,
            $this->value != "" ? $this->value : $this->model->{$this->attribute},
            $this->model->hasError($this->attribute) ? 'is-invalid' : '',

        );
    }
}
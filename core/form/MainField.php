<?php


namespace app\core\form;


use app\core\Model;

abstract class MainField
{
    public Model $model; //modeli aliriq
    public string $attribute; // modelde olan fieldlarin adini aliriq

    /**
     * Field constructor.
     * @param Model $model
     * @param string $attribute
     */
    public function __construct(Model $model, string $attribute)
    {
        $this->model = $model;
        $this->attribute = $attribute;
    }

    abstract public function renderInput(): string; // select ,input ve s


    public function __toString(): string // html yazdirmaq ucun istifade olunur
    {
        return sprintf('
            <div class="form-group">
                <label>%s</label>
                %s
                <div class="invalid-feedback">%s</div>
            </div>
        ',
            $this->model->labels()[$this->attribute] ?? $this->attribute,
            $this->renderInput(),
            $this->model->getError($this->attribute)

        );
    }
}
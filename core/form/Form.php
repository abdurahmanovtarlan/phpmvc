<?php


namespace app\core\form;


use app\core\database\DatabaseModel;
use app\core\Model;

class Form
{

    public static function begin($params = [])
    {
        echo sprintf('<form action="%s" method="%s" class="%s" enctype="multipart/form-data">', $params['url'], $params['method'], $params['class'] ?? '');
        return new self();
    }

    public static function end()
    {
        echo "</form>";
    }

    public function field(Model $model, $attribute, $value = '')
    {
        return new InputField($model, $attribute, $value);
    }
}
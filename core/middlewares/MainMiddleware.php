<?php


namespace app\core\middlewares;

abstract class MainMiddleware
{

    abstract public function execute();
}
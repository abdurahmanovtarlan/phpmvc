<?php

namespace app\core;

use app\controllers\AuthController;
use app\core\exception\ForbiddenException;
use app\core\exception\NotFoundException;

/**
 * Class Router
 * @package app\core
 */
class Router
{
    public static $patterns = [
        ':id[0-9]?' => '([0-9]+)',
        ':url[0-9]?' => '([0-9a-zA-Z-_]+)',
    ];
    public static bool $has_route = false;
    protected array $routes = [];
    public Request $request;
    public Response $response;
    protected bool $api = false;


    public function __construct(Request $request, Response $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function get($path, $callback)
    {
        $this->routes['get'][$path] = $callback;
    }

    public function post($path, $callback)
    {
        $this->routes['post'][$path] = $callback;
    }

//    public function resolve()
//    {
//        $path = $url = $this->request->getPath(); // url yeri goturur
//        $method = $this->request->method(); // methodu goturur
//        $checkApi = trim($path, '/');
//        $defaultURL = null;
//        foreach ($this->routes[$method] as $path => $props) {
//            $defaultURL = $path;
//            foreach (self::$patterns as $key => $pattern) {
//                $path = preg_replace('#' . $key . '#', $pattern, $path);
//            }
//            $pattern = '#^' . $path . '$#';
//            print_r($pattern . "   ");
//            print_r(preg_match($pattern, $defaultURL, $params) . "<br>");
//            if (!preg_match($pattern, $defaultURL, $params)) {
//                array_shift($params);
//            }
//        }
//        if (str_contains($checkApi, 'api')) {
//            $this->api = true;
//            header("Content-Type: application/json; charset=UTF-8");
//        }
//        $callback = $this->routes[$method][$path] ?? false;// bu verilenlere uygun callbacki goturur
//        if ($callback === false) {
////            if ($this->api) {
////                header("Content-Type: text/html; charset=UTF-8");
////                $this->response->setStatusCode(404);
////            }
//            //return $this->renderView('404');
//            throw new NotFoundException();
//        }
//        if (is_string($callback)) {
//            return Application::$app->view->renderView($callback);
//        }
//        if (is_array($callback)) {
//            /**
//             * @var Controller $controller
//             */
////            $callback[0] = new $callback[0]();
//            $controller = new $callback[0]();
//            Application::$app->controller = $controller;
//            $controller->action = $callback[1];
//            $callback[0] = $controller;
//            foreach ($controller->getMiddlewares() as $middleware) {
//                $middleware->execute();
//            }
//        }
////        return call_user_func([new $callback[0](), $callback[1]]); // callback funksiyasini cagirir
//        return call_user_func($callback, $this->request, $this->response); // callback funksiyasini cagirir
//    }


    public function resolve()
    {

        $path = $url = $this->request->getPath(); // url yeri goturur
        $method = $this->request->method(); // methodu goturur
        $defaultURL = null;
        foreach ($this->routes[$method] as $path => $props) {
            self::$has_route = true;
            $defaultURL = $path;
            foreach (self::$patterns as $key => $pattern) {
                $path = preg_replace('#' . $key . '#', $pattern, $path);
            }
            $pattern = '#^' . $path . '$#';
            if (preg_match($pattern, $url, $params)) {
                self::$has_route = true;
                array_shift($params);
                $callback = $this->routes[$method][$defaultURL] ?? false;
                if (is_string($callback)) {
                    return Application::$app->view->renderView($callback);
                }
                if (is_array($callback)) {
                    $callback[0] = new $callback[0]();
                    $controller = new $callback[0]();
                    Application::$app->controller = $controller;
                    $controller->action = $callback[1];
                    $callback[0] = $controller;
                    foreach ($controller->getMiddlewares() as $middleware) {
                        $middleware->execute();
                    }
                }
                return call_user_func_array([$callback[0], $callback[1]], [$this->request, $this->response, isset($params[0]) ? $params[0] : '']);
            } else {
                $callback = $this->routes[$method][$url] ?? false;
                self::$has_route = false;
            }
        }
        self::hasRoute();
    }

    public static function hasRoute()
    {
        if (self::$has_route === false) {
            throw new NotFoundException();
        }
    }

}
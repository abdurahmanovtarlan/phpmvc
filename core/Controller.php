<?php


namespace app\core;


use app\core\middlewares\MainMiddleware;

class Controller
{

    public string $layout = 'main';
    public string $action = '';
    /**
     * @var \app\core\middlewares\MainMiddleware[]
     */
    protected array $middlewares = [];

    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    public function render($view, $params = [])
    {
        return Application::$app->view->renderView($view, $params);
    }



    public function middleware(MainMiddleware $middleware)
    {
        $this->middlewares[] = $middleware;
    }

    /**
     * @return MainMiddleware[]
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }
}
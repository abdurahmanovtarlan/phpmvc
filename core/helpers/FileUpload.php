<?php

namespace app\core\helpers;

class FileUpload
{
    public static function upload($file, $folderName)
    {
        $filename = uniqid() . $file['name'];
        $tempname = $file['tmp_name'];
        $folder = $folderName . '/' . $filename;
        if (move_uploaded_file($tempname, $folder)) {
            $file = $_SERVER['HTTP_ORIGIN'] . '/images/' . $filename;
        }
        return $file;
    }
}
<?php


namespace app\core;


use app\core\exception\NotFoundException;

class Pagination
{
    private $db;
    private int $limit;
    private int $page;
    private $query;
    private int $total;


    /**
     * Pagination constructor.
     * @param $db
     * @param $query
     */
    public function __construct($query)
    {
        $this->db = Application::$app->db;
        $this->query = $query;
        $rs = $this->db->prepare($this->query);
        $rs->execute();
        $this->total = $rs->rowCount();
    }

    /**
     * @param int $page
     * @param int $limit
     * @return \stdClass
     */
    public function getData($page, int $limit)
    {

        $this->limit = $limit;
        $this->page = $page;
        $last = ceil($this->total / $this->limit);
        if ($page > $last) {
            $this->page = $last;
        }
        $query = $this->query . " LIMIT " . (($this->page - 1) * $this->limit < 0 ? 0 : ($this->page - 1) * $this->limit) . "," . $this->limit;
        $rs = $this->db->prepare($query);
        $rs->execute();
        $results = [];
        while ($row = $rs->fetch(\PDO::FETCH_OBJ)) {
            $results[] = $row;
        }


        $result = new \stdClass();
        $result->page = $this->page;
        $result->limit = $this->limit;
        $result->total = $this->total;
        $result->data = $results;

        return $result;
    }

    /**
     * @param $links
     * @param $class
     * @return string
     */
    public function links($links, $class)
    {
        $last = ceil($this->total / $this->limit);
        $start = (($this->page - $links) > 0) ? $this->page - $links : 1;
        $end = (($this->page + $links) < $last) ? $this->page + $links : $last;

        $html = '<ul class="' . ($end == 1 ? 'd-none ' : "") . $class . '">';
        $disabled = ($this->page == 1) ? 'disabled' : '';
        $html .= '<li class="page-item ' . $disabled . '"><a class="page-link" href="?page=' . ($this->page - 1) . '">Əvvəlki</a></li>';
        if ($start > 1) {
            $html .= '<li class="page-item"><a class="page-link" href="?page=1">1</a></li>';
            $html .= '<li  class="disabled page-item"><span>...</span></li>';
        }

        for ($i = $start; $i <= $end; $i++) {
            $disabled = ($this->page == $i) ? "active" : "";
            $html .= '<li class="page-item ' . $disabled . '"><a class="page-link" href="?page=' . $i . '">' . $i . '</a></li>';
        }

        if ($end < $last) {
            $html .= '<li class="page-item disabled"><span>...</span></li>';
            $html .= '<li class="page-item"><a class="page-link ' . $disabled . '" href="?page=' . $last . '">' . $last . '</a></li>';
        }

        $disabled = ($this->page == $last) ? "disabled" : "";
        $html .= '<li class="page-item ' . $disabled . '"><a class="page-link ' . $disabled . '" href="?page=' . ($this->page + 1) . '">Növbəti</a></li>';
        $html .= '</ul>';
        return $html;

    }
}












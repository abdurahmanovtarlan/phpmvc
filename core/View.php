<?php


namespace app\core;


class View
{

    public string $title = '';

    public function renderView($view, $params = [])
    {
        $viewContent = $this->renderOnlyView($view, $params);
        $layoutContent = $this->layoutContent();
        return str_replace('{{content}}', $viewContent, $layoutContent); // main deki content parametri ile view leri evez edir
    }

    public function renderContent($viewContent)
    {
        $layoutContent = $this->layoutContent();
        return str_replace('{{content}}', $viewContent, $layoutContent); // main deki content parametri ile view leri evez edir
    }

    /**
     * bu funksiya umumi html taglarini ozunde saxlayir
     * @return false|string
     */
    protected function layoutContent()
    {
        $layout = Application::$app->layout;
        if (isset(Application::$app->controller)) {
            $layout = Application::$app->controller->layout;
        }
        ob_start();
        include_once Application::$ROOT_DIR . "/views/layouts/$layout.php";
        return ob_get_clean();
    }

    /**
     * bu funksiya diger gorunus fayllarini include etmek ucundur.
     * @param $view
     * @return false|string
     */
    protected function renderOnlyView($view, $params)
    {
        foreach ($params as $key => $value) {
            $$key = $value; // $$key value cevrilir ve birbasa php faylinda keyle cagirmaq olur
            // ['name'=>'php'];
            // $name = 'php'; olur
        }
        ob_start();
        include_once Application::$ROOT_DIR . "/views/$view.php";
        return ob_get_clean();
    }
}
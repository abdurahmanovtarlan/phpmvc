<?php


namespace app\core;


abstract class Model
{
    public const REQUIRED = 'required';
    public const EMAIL = 'email';
    public const MIN = 'min';
    public const MAX = 'max';
    public const MATCH = 'match';
    public const UNIQUE = 'unique';
    public array $errors = [];

    public function loadData($data)
    {
        foreach ($data as $key => $value) {
            if (property_exists($this, $key)) { // bu sinifde sinife aid parametrlerin yoxlayir
                if (is_array($value)) {
                    $this->{$key} = json_encode($value);
                } else {
                    $this->{$key} = $value; //keye value deyeri menimsedilir
                }
            }
        }
    }

    abstract public function rules(): array;

    public function labels(): array
    {
        return [];
    }

    public function validate()
    {
        foreach ($this->rules() as $attribute => $rules) {
            $value = $this->{$attribute};
            foreach ($rules as $rule) {
                $ruleName = $rule;
                if (!is_string($ruleName)) {
                    $ruleName = $rule[0];
                }
                if ($ruleName === self::REQUIRED && !$value) {
                    $this->addErrorForRule($attribute, self::REQUIRED);
                }
                if ($ruleName === self::EMAIL && !filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $this->addErrorForRule($attribute, self::EMAIL);
                }
                if ($ruleName === self::MIN && strlen($value) < $rule['min']) {
                    $this->addErrorForRule($attribute, self::MIN, $rule);
                }
                if ($ruleName === self::MAX && strlen($value) > $rule['max']) {
                    $this->addErrorForRule($attribute, self::MAX, $rule);
                }
                if ($ruleName === self::MATCH && $value !== $this->{$rule['match']}) {
                    $this->addErrorForRule($attribute, self::MATCH, $rule);
                }
                if ($ruleName === self::UNIQUE) {
                    $className = $rule['class'];
                    $uniqueAttr = $rule['attribute'] ?? $attribute;
                    $tableName = $className::tableName();
                    $statement = Application::$app->db->pdo->prepare("SELECT * FROM $tableName WHERE $uniqueAttr = :$uniqueAttr");
                    $statement->bindValue(":$uniqueAttr", $value);
                    $statement->execute();
                    $record = $statement->fetchObject();
                    if ($record) {
                        $this->addErrorForRule($attribute, self::UNIQUE, ['field' => $attribute]);
                    }
                }
            }
        }
        return empty($this->errors);

    }

    private function addErrorForRule(string $attr, string $rule, $params = [])
    {
        $message = $this->errorMessages()[$rule] ?? ''; // qaydalara gore
        foreach ($params as $key => $value) {
            $message = str_replace("{{$key}}", $value, $message);
        }
        $this->errors[$attr][] = $message; //
    }

    public function addError(string $attr, string $message)
    {
        $this->errors[$attr][] = $message; //
    }

    public function errorMessages()
    {
        return [
            self::REQUIRED => 'Bu xananın doldurulması məcburidir',
            self::EMAIL => 'Bu sahə etibarlı bir e-poçt ünvanı olmalıdır',
            self::MIN => 'Bu sahənin minimum uzunluğu {min} olmalıdır',
            self::MAX => 'Bu sahənin maksimum uzunluğu {max} olmalıdır',
            self::MATCH => 'Bu sahə {match} eyni olmalıdır',
            self::UNIQUE => 'Bu {field} məlumat artıq mövcuddur',
        ];
    }

    public function hasError($field)
    {
        return $this->errors[$field] ?? false;
    }

    public function getError($attr)
    {
        return $this->errors[$attr][0] ?? false;
    }


}
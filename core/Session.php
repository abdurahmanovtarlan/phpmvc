<?php


namespace app\core;


class Session
{
    public const SESSION_KEY = 'messages';

    public function __construct()
    {
        session_start();
        $sessionMessages = $_SESSION[self::SESSION_KEY] ?? [];
        foreach ($sessionMessages as $key => $sessionMessage) {
            $sessionMessages[$key]['see'] = true;
        }
        $_SESSION[self::SESSION_KEY] = $sessionMessages;
    }

    public function setSession($key, $message)
    {
        $_SESSION[self::SESSION_KEY][$key] = [
            'see' => false,
            'value' => $message
        ];
    }

    public function getSession($key)
    {
        return $_SESSION[self::SESSION_KEY][$key]['value'] ?? false;
    }

    public function set($key, $value)
    {
        $_SESSION[$key] = "$value";
    }

    public function get($key)
    {
        return $_SESSION[$key] ?? false;
    }

    public function remove($key)
    {
        unset($_SESSION[$key]);
    }

    public function __destruct()
    {
        $sessionMessages = $_SESSION[self::SESSION_KEY] ?? [];
        foreach ($sessionMessages as $key => $sessionMessage) {
            if ($sessionMessage['see']) {
                unset($sessionMessages[$key]);
            }
        }
        $_SESSION[self::SESSION_KEY] = $sessionMessages;
    }

}
<?php


namespace app\core;


/**
 * Class Request
 * @package app\core
 */
class Request
{
    /**
     * @return false|mixed|string
     */
    public function getPath()
    {
//        $path = $_SERVER['REQUEST_URI'] ?? '/';
        $path = explode('phpspace', $_SERVER['REQUEST_URI'])[1] ?? $_SERVER['REQUEST_URI'];
        $position = strpos($path, "?"); // ? yerini teyin edir
        if ($position === false) {
            return $path;
        }
        return substr($path, 0, $position); // ?-la qeder olan stringi goturur


    }

    /**
     * @return string
     */
    public function method(): string
    {
        return strtolower($_SERVER['REQUEST_METHOD']); // get ve ya post methodu oldugunu goturur
    }

    public function isGET()
    {
        return $this->method() == 'get';
    }

    public function isPOST()
    {
        return $this->method() == 'post';
    }

    /**
     * @return array
     */
    public function getBody(): array
    {
        $body = [];
        if ($this->method() === 'get') {
            foreach ($_GET as $key => $value) {
                $body[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        if ($this->method() === 'post') {
            foreach ($_POST as $key => $value) {
                $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            if (isset($_FILES) && !empty($_FILES)) {
                foreach ($_FILES as $key => $value) {
                    if (!empty($value['name']) && isset($value['name'])) {
                        $body[$key] = $value;
                    }
                }
            }
        }
        return $body;
    }

    /**
     * @param array $body
     * @return array
     */
    public function input(array $body = []): array
    {
        if ($this->method() === 'get') {
            foreach ($_GET as $key => $value) {
                $body[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
        if ($this->method() === 'post') {
            foreach ($_POST as $key => $value) {
                $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
            if (isset($_FILES) && !empty($_FILES)) {
                foreach ($_FILES as $key => $value) {
                    if (!empty($value['name']) && isset($value['name'])) {
                        $body[$key] = $value;
                    }
                }
            }
        }
        return $body;
    }
}
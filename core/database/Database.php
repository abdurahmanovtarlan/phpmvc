<?php


namespace app\core\database;


use app\core\Application;

class Database
{

    public \PDO $pdo;


    public function __construct(array $config)
    {
        $driver = $config['driver'] ?? '';
        $database = $config['database'] ?? '';
        $port = $config['port'] ?? '';
        $host = $config['host'] ?? '';
        $user = $config['username'] ?? '';
        $password = $config['password'] ?? '';
        $dsn = "$driver:host=$host;dbname=$database";
        $this->pdo = new \PDO($dsn, $user, $password);
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }


    public function applyMigrations()
    {
        $this->createMigrationsTable();
        $appliedMigrations = $this->getAppliedMigrations();
        $files = scandir(Application::$ROOT_DIR . '/migrations');
        $toApplyMigrations = array_diff($files, $appliedMigrations);

        $newMigrations = [];
        foreach ($toApplyMigrations as $migration) {
            if ($migration === '.' || $migration === '..') {
                continue;
            }

            require_once Application::$ROOT_DIR . '/migrations/' . $migration;
            $className = pathinfo($migration, PATHINFO_FILENAME);
            $instance = new $className();
            $this->message("Migrating: $migration");
            $instance->up();
            $this->message("Migrated:  $migration");
            $newMigrations[] = $migration;
        }
        if (!empty($newMigrations)) {
            $this->saveMigrations($newMigrations);
        } else {
            $this->message("Yeni migrate yoxdur");
        }
    }

    public function createMigrationsTable()
    {
        $this->pdo->exec("CREATE TABLE IF NOT EXISTS migrations (
                id INT AUTO_INCREMENT PRIMARY  KEY ,
                migration VARCHAR(255),
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP 
            ) ENGINE=INNODB;");
    }

    public function getAppliedMigrations()
    {
        $statement = $this->pdo->prepare("SELECT migration FROM migrations");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function saveMigrations(array $migrations)
    {
        $str = implode(',', array_map(fn($m) => "('$m')", $migrations));
        $statement = $this->pdo->prepare("INSERT INTO migrations (migration) VALUE $str");
        $statement->execute();
    }


    public function prepare($sql)
    {
        return $this->pdo->prepare($sql);
    }

    public function message($message)
    {
        echo $message . " - [" . date('Y-m-d H:i:s') . "]" . PHP_EOL;
    }

}
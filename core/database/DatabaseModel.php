<?php


namespace app\core\database;


use app\core\Application;
use app\core\exception\NotFoundException;
use app\core\Model;
use app\core\Pagination;
use app\models\User;

abstract class DatabaseModel extends Model
{
    public static array $where = [];
    public static array $orderby = [];
    public static string $table;
    private static string $selectSQL = '*';
    private static string $deleteSQL = '';
    private static string $updateSQL = '';
    protected static string $sql = '';

    abstract static public function tableName(): string;

    abstract public function attributes(): array;

    abstract static public function primaryKey(): string;


    public function save()
    {
        try {
            $tableName = $this->tableName();
            $attributes = $this->attributes();
            $params = array_map(fn($attr) => ":$attr", $attributes);
            $statement = self::prepare("INSERT INTO $tableName (" . implode(',', $attributes) . ") VALUES (" . implode(',', $params) . ")");
            foreach ($attributes as $attr) {
                $statement->bindValue(":$attr", $this->{$attr});
            }
            $statement->execute();
            return true;
        } catch (\Exception $e) {
            echo Application::$app->router->renderContent($e->getMessage());
        }

    }

    public static function prepare($sql)
    {
        return Application::$app->db->pdo->prepare($sql);
    }

    protected static function prepareSql($action, $sql = '')
    {
        if ($action == 'select') {
            self::$sql = sprintf("SELECT %s FROM %s", self::$selectSQL, static::tableName());
        }
        if ($action == 'insert') {
            self::$sql = $sql;
        }
        if ($action == 'delete') {
            self::$sql = sprintf(" DELETE FROM % s WHERE % s", static::tableName(), !empty(self::$deleteSQL) ? self::$deleteSQL : implode(self::$where));
        }
//        if (count(self::$where) && $action != 'delete') {
//            self::$sql .= " WHERE " . implode(self::$where);
//        }
        if (!empty(self::$orderby)) {
            self::$sql .= " ORDER BY " . implode(self::$orderby);
        }
    }

    public static function all()
    {
        $tableName = static::tableName();
        $statement = static::prepare("SELECT * FROM $tableName");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_OBJ);
    }

    public static function orderByGet($attr, $key)
    {
        $tableName = static::tableName();
        $statement = static::prepare("SELECT * FROM $tableName ORDER  BY $attr $key");
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_OBJ);
    }

//    public static function where($where) // [email=> test@gmail.com,firstname=> php]
//    {
//        $tableName = static::tableName();
//        $attributes = array_keys($where);
//        $sql = implode("AND", array_map(fn($attr) => "$attr = :$attr", $attributes));
//        $statement = self::prepare("SELECT * FROM $tableName WHERE $sql");
//        foreach ($where as $key => $item) {
//            $statement->bindValue(":$key", $item);
//        }
//        $statement->execute();
//        return $statement->fetchAll(\PDO::FETCH_ASSOC);'
//    }

//    public static function delete($id = null)
//    {
//        $tableName = static::tableName();
//        if (!empty($id)) {
//            $value = static::class::findOne(['id' => $id]);
//            if ($value) {
//                $sql = "DELETE FROM $tableName  WHERE id=:id";
//                $statement = self::prepare($sql);
//                $statement->bindValue(":id", $id);
//                $statement->execute();
//                return 1;
//            } else {
//                return 0;
//            }
//        }
//        return -1;
//    }

    public static function findOne($where)
    {
        $tableName = static::tableName();
        $attributes = array_keys($where);
        $sql = implode("AND", array_map(fn($attr) => "$attr = :$attr", $attributes));
        // select * from $tableName where email = :email AND firtname = :firstname;
        $statement = Application::$app->db->pdo->prepare("SELECT * FROM $tableName WHERE $sql");
        foreach ($where as $key => $item) {
            $statement->bindValue(":$key", $item);
        }
        $statement->execute();
        return $statement->fetchObject(User::class); // User class
    }


    public function get()
    {

        $this->prepareSql("select");
        return $this->executeQuery();
    }

    public function first()
    {
        $this->prepareSql("select");
        $query = $this->db->prepare($this->sql);
        $query->execute();
        return $query->fetch(\PDO::FETCH_OBJ);
    }

    protected static function executeQuery($action = "", $sql = '')
    {
        self::$sql .= $sql;
        $statement = Application::$app->db->prepare(self::$sql);
        $statement->execute();
        if ($action == "delete" || $action == 'update') {
            return 1;
        }
        if ($action == "find") {
            return $statement->fetch(\PDO::FETCH_OBJ);
        }
        return $statement->fetchAll(\PDO::FETCH_OBJ);
    }

    public static function select($sql = "*")
    {
        self::$selectSQL = $sql;
        return new static();
    }

    public function create($table = "")
    {
        $tableName = $table ? $table : static::tableName();
        try {
            $attributes = $this->attributes();
            $params = array_map(fn($attr) => ":$attr", $attributes);
            $sql = "INSERT INTO $tableName (" . implode(',', $attributes) . ") VALUES(" . implode(',', $params) . ")";
            $statement = self::prepare($sql);
            foreach ($attributes as $attr) {
                $statement->bindValue(":$attr", htmlspecialchars($this->{$attr}));
            }
            $statement->execute();
            return true;
        } catch (\Exception $e) {
            echo Application::$app->router->renderContent($e->getMessage());
        }
    }

    public function update($data = [])
    {
        $sql = "";
        $comma = " ";
        foreach ($data as $key => $val) {
            if (!empty($val)) {
                $sql .= $comma . $key . " = '" . trim($val) . "'";
                $comma = ", ";
            } else {
                $this->errors[$key] = '';
            }
        }
        $sql = sprintf(" UPDATE %s SET %s WHERE %s", static::tableName(), $sql, implode(self::$where));
        return static::executeQuery('update', $sql);
    }

    public static function delete()
    {
        self::prepareSql('select');
        $has = self::executeQuery();
        if (count($has)) {
            self::prepareSql('delete');
            return static::executeQuery('delete');
        } else {
            throw new NotFoundException();
        }
    }

    public static function deleteId($id)
    {
        self::prepareSql('delete');
        $has = self::executeQuery();
        if (count($has)) {
            self::$deleteSQL = " id = " . $id;
            self::prepareSql('delete');
            return self::executeQuery('delete');
        } else {
            return 0;
        }
    }

    /**
     * @param $attr
     * @param string $key
     * @return static
     */
    public static function orderBy($attr, $key = "ASC")
    {
        static::$orderby[$attr] = $attr . " " . $key;
        return new static();
    }

    /**
     * @param $column
     * @param $value
     * @param string $operation
     * @return static
     */
    public static function where($column, $value, $operation = '=')
    {
        self::$where[$column] = $column . ' ' . $operation . ' "' . $value . '"';
        return new static();
    }

    public static function findOrFail($column, $value)
    {
        $sql = " WHERE " . $column . ' = "' . $value . '"';
        self::prepareSql('select');
        return self::executeQuery('find', $sql);

    }

    public function query()
    {
        self::prepareSql('select');
        return self::$sql;
    }

    public function paginate($limit = 1)
    {
        self::prepareSql('select');
        return new Pagination(self::$sql);
    }


}
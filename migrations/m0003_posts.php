<?php


class m0003_posts
{
    public function up()
    {
        $db = \app\core\Application::$app->db;

        $SQL = "CREATE TABLE posts ( 
                    id INT AUTO_INCREMENT PRIMARY KEY, 
                    user_id INTEGER NOT NULL,
                    title VARCHAR(255) NOT NULL,
                    detail VARCHAR(255) NOT NULL,
                    image VARCHAR(255) NOT NULL,
                    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)
                    ENGINE=INNODB;";
        $db->pdo->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->db;

        $SQL = "DROP TABLE POSTS;";
        $db->pdo->exec($SQL);
    }
}
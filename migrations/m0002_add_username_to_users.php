<?php


class m0002_add_username_to_users
{
    public function up()
    {
        $db = \app\core\Application::$app->db;

        $SQL = "ALTER TABLE users ADD COLUMN username VARCHAR(255) NULL;";
        $db->pdo->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->db;

        $SQL = "ALTER TABLE users DROP COLUMN username;";
        $db->pdo->exec($SQL);
    }
}
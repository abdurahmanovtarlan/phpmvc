<?php


namespace app\controllers;


use app\core\Application;
use app\core\Controller;
use app\core\helpers\FileUpload;
use app\core\middlewares\AuthMiddleware;
use app\core\Request;
use app\core\Response;
use app\core\validation\Validation;
use app\models\Post;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware(new AuthMiddleware(['index']));
    }

    /**
     * @return string|string[]
     */
    public function index()
    {
        $user_id = Application::$app->user->id;
        $posts = Post::where('user_id', $user_id)->orderBy('id', 'DESC')->paginate();
        $params = [
            'posts' => $posts
        ];

        return $this->render('post', $params);
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return string|string[]
     */
    public function post(Request $request, Response $response)
    {
        $postForm = new Post();
        if ($request->isPOST()) {
            $postForm->loadData($request->getBody());
            if ($postForm->validate() && $postForm->save()) {
                Application::$app->session->setSession('success', 'Thanks for contact us');
                $response->redirect('/posts');
            }
        }
        return $this->render('postcreate', [
            'model' => $postForm,
        ]);
    }

    public function update(Request $request, Response $response, $id)
    {

        $postForm = Post::findOrFail('id', $id);

        return $this->render('update', [
            'model' => $postForm,
            'id' => $id,
        ]);
    }

    public function updatePost(Request $request, Response $response, $id)
    {
        $validation = new Validation();
        $validation->validate([
            'title' => $request->getBody()['title'],
            'detail' => $request->getBody()['detail']
        ], [
            'title' => ['required'],
            'detail' => ['required']
        ]);
        if (!$validation->error()) {
            $image = '';
            if (isset($request->getBody()['image'])) {
                $image = FileUpload::upload($request->getBody()['image'], 'images');
            }
            Post::where('id', $id)->update([
                'title' => $request->getBody()['title'],
                'detail' => $request->getBody()['detail'],
                'image' => $image,
            ]);
            Application::$app->session->setSession('success', 'Thanks for contact us');
            $response->redirect('/posts');
        } else {
            $postForm = Post::findOrFail('id', $id);
            $postForm->title = $request->getBody()['title'];
            $postForm->detail = $request->getBody()['detail'];
            return $this->render('update', [
                'model' => $postForm,
                'id' => $id,
                'validation' => $validation->errors,
            ]);
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     * @throws \app\core\exception\NotFoundException
     */
    public
    function delete(Request $request, Response $response)
    {
        $post = Post::where('id', $request->getBody()['id'])->delete();
        Application::$app->session->setSession('success', 'Post successfully deleted');
        $response->back();

    }
}
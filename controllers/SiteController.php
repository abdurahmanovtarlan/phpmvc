<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\database\Database;
use app\core\Request;
use app\core\Response;
use app\core\UserModel;
use app\models\ContactForm;
use app\models\User;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    public function home()
    {
        $params = [
            'name' => 'Tarlan',
        ];
        return $this->render('home', $params);
    }

    public function contact(Request $request, Response $response)
    {
        $contact = new ContactForm();
        if ($request->isPOST()) {
            $contact->loadData($request->getBody());
            if ($contact->validate() && $contact->send()) {
                Application::$app->session->setSession('success', 'Thanks for contact us');
                $response->redirect('/contact');
            }
        }
        return $this->render('contact', [
            'model' => $contact
        ]);
    }




}
<?php


namespace app\controllers;


use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\Request;
use app\core\Response;
use app\models\LoginForm;
use app\models\User;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware(new AuthMiddleware(['profile']));
    }

    public function login(Request $request, Response $response)
    {
        $this->setLayout('auth');
        $loginForm = new LoginForm();
        if ($request->isPOST()) {
            $loginForm->loadData($request->getBody());
            if ($loginForm->validate() && $loginForm->login()) {
                $response->redirect('/');
            }
        }
        return $this->render('login', [
            'model' => $loginForm,
        ]);
    }

    public function register(Request $request)
    {
        $this->setLayout('auth');

        $user = new User();
        if ($request->isPOST()) {
            $user->loadData($request->getBody());
            if ($user->validate() && $user->save()) {
                Application::$app->session->setSession('success', 'Thanks for registering');
                $user = User::findOne(['email' => $user->email]);
                Application::$app->login($user);
                Application::$app->response->redirect('/');
                exit;
            }
            return $this->render('register', [
                'model' => $user // kohne deyerleri saxlamaq ucun
            ]);
        }
        return $this->render('register', [
            'model' => $user
        ]);
    }

    public function logout(Request $request, Response $response)
    {
        Application::$app->logout();
        $response->redirect('/');
    }

    public function profile()
    {
        return $this->render('profile');
    }


    /**
     * @param Request $request
     * @return false|string
     */
    public function users(Request $request)
    {
        $users = User::all();
        return json_encode(['users' => $users]);
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function userAdd(Request $request)
    {
        $user = new User();
        if ($request->isPOST()) {
            $user->loadData($request->getBody());
            if ($user->validate() && $user->save()) {
                return json_encode(['status' => true, 'user' => $user]);
            } else {
                return json_encode($user->errors);
            }
        }
        return empty($user->errors);
    }

    public function userDelete(Request $request)
    {
        $id = $request->getBody()['id'] ?? '';
        $user = User::delete($id);
        if ($user == 1) {
            return json_encode(['status' => true, 'message' => 'User silindi']);
        } elseif ($user == 0) {
            return json_encode(['status' => false, 'message' => 'User tapilmadi']);
        } else {
            return json_encode(['status' => false, 'message' => 'Id daxil edin']);

        }
    }

}
<?php

/**
 * @var $model \app\models\Post
 */

?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <?php
            $form = \app\core\form\Form::begin(['url' => "/post/update/" . $model->id, 'method' => 'post']);
            ?>
            <div class="card my-5 shadow-sm">
                <div class="card-header">
                    <h5>Post Update</h5>
                </div>
                <div class="card-body">
                    <input type="hidden" value="<?php echo $model->id; ?>" id="id" name="id">
                    <div class="form-group">
                        <label for="name">Title:</label>
                        <input type="text" name="title" id="title" value="<?php echo $model->title; ?>"
                               class="form-control <?php echo isset($validation['title']) ? 'is-invalid' : '' ?>">
                        <div class="invalid-feedback">
                            <?php
                            echo isset($validation['title']) ? $validation['title'][0] : '';
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Detail:</label>
                        <input type="text" name="detail" id="detail" value="<?php echo $model->detail; ?>"
                               class="form-control <?php echo isset($validation['detail']) ? 'is-invalid' : '' ?>"">
                        <div class="invalid-feedback">
                            <?php
                            echo isset($validation['detail']) ? $validation['detail'][0] : '';
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Image: <a href="<?php echo $model->image; ?>">Image Show</a>
                        </label>
                        <input type="file" name="image" id="image" class="form-control">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mt-2 w-100">Submit</button>
                </div>
            </div>
            <?php \app\core\form\Form::end(); ?>
        </div>
    </div>
</div>
<?php

/**
 * @var $model \app\models\Post
 */
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <?php $form = \app\core\form\Form::begin(['url' => "/post/create", 'method' => 'post']); ?>
            <div class="card my-5 shadow-sm">
                <div class="card-header">
                    <h5>Post Create</h5>
                </div>
                <div class="card-body">
                    <?php echo $form->field($model, 'title'); ?>
                    <?php echo new \app\core\form\TextareaField($model, 'detail'); ?>
                    <?php echo $form->field($model, 'image')->file(); ?>

                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary mt-2 w-100">Submit</button>
                </div>
            </div>
            <?php \app\core\form\Form::end(); ?>
        </div>
    </div>
</div>
<div class="container  mt-3">
    <div class="d-flex align-items-center justify-content-between">
        <h3>Posts</h3>
        <a href="/post/create" class="btn btn-primary btn-sm">Add Post</a>
    </div>
    <hr>
    <div class="row">
        <?php
        $page = isset($_GET['page']) && $_GET['page'] > 0 ? $_GET['page'] : 1;
        foreach ($posts->getData($page, 9)->data as $post) { ?>
            <div class="col-md-4">
                <div class="card mb-3">
                    <div class="card-header">
                        <div class="d-flex align-items-center justify-content-between">
                            <h6 class="card-title"><?php echo $post->title; ?>
                            </h6>
                            <form action="/post/delete" method="post">
                                <a href="/post/update/<?php echo $post->id; ?>" class="btn btn-info btn-sm">Edit</a>
                                <input type="hidden" name="id" id="name" value="<?php echo $post->id; ?>">
                                <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                            </form>
                        </div>

                    </div>
                    <img class="card-img" src="<?php echo $post->image; ?>" alt="">
                    <div class="card-body">
                        <?php echo $post->detail; ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <nav>
        <?php
            echo $posts->links(7, 'pagination');
        ?>
    </nav>
</div>
<?php

/**
 * @var $this \app\core\View;
 * @var $model \app\models\ContactForm;
 */

use app\core\form\TextareaField;

$this->title = 'Contact';
?>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <?php $form = \app\core\form\Form::begin(['url' => '', 'method' => 'post', 'class' => 'card mt-5 shadow-sm']) ?>
            <div class="card-body">
                <h3 class="card-title">Contact</h3>
                <div class="mb-3">
                    <?php echo $form->field($model, 'subject'); ?>
                </div>
                <div class="mb-3">
                    <?php echo $form->field($model, 'email'); ?>
                </div>
                <div class="mb-3">
                    <?php echo new TextareaField($model, 'body'); ?>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>

            <?php \app\core\form\Form::end(); ?>
        </div>
    </div>
</div>

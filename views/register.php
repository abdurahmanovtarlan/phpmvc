<?php

/**
 * @var $model \app\models\User
 */
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <?php $form = \app\core\form\Form::begin(['url' => "/register", 'method' => 'post']); ?>
            <div class="card my-5 shadow-sm">
                <div class="card-body">
                    <h5>Register</h5>
                    <?php echo $form->field($model, 'firstname'); ?>
                    <?php echo $form->field($model, 'lastname'); ?>
                    <?php echo $form->field($model, 'email'); ?>
                    <?php echo $form->field($model, 'password')->password(); ?>
                    <?php echo $form->field($model, 'confirmPassword')->password(); ?>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary mt-2 w-100">Submit</button>
                    </div>
                </div>
            </div>
            <?php \app\core\form\Form::end(); ?>

            <!--            <form class="card my-5 shadow-sm" action="" method="post">-->
            <!---->
            <!---->
            <!--                <div class="card-body">-->
            <!--                    <h5 class="card-title">Register</h5>-->
            <!--                    <hr>-->
            <!---->
            <!--                    <div class="mb-3">-->
            <!--                        <label class="form-label">First name</label>-->
            <!--                        <input type="text"-->
            <!--                               class="form-control -->
            <?php //echo $model->hasError('firstname') ? 'is-invalid' : ''; ?><!--"-->
            <!--                               id="firstname" value="--><?php //echo $model->firstname; ?><!--"-->
            <!--                               name="firstname"/>-->
            <!--                        <div class="invalid-feedback">-->
            <!--                            --><?php //echo $model->getError('firstname'); ?>
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                    <div class="mb-3">-->
            <!--                        <label class="form-label">Last name</label>-->
            <!--                        <input type="text"-->
            <!--                               class="form-control -->
            <?php //echo $model->hasError('lastname') ? 'is-invalid' : ''; ?><!--"-->
            <!--                               id="lastname"-->
            <!--                               value="--><?php //echo $model->lastname ?><!--" name="lastname"/>-->
            <!--                        <div class="invalid-feedback">-->
            <!--                            --><?php //echo $model->getError('lastname'); ?>
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                    <div class="mb-3">-->
            <!--                        <label class="form-label">Email</label>-->
            <!--                        <input type="email"-->
            <!--                               class="form-control -->
            <?php //echo $model->hasError('email') ? 'is-invalid' : ''; ?><!--"-->
            <!--                               id="email"-->
            <!--                               value="--><?php //echo $model->email; ?><!--" name="email"/>-->
            <!--                        <div class="invalid-feedback">-->
            <!--                            --><?php //echo $model->getError('email'); ?>
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                    <div class="mb-3">-->
            <!--                        <label class="form-label">Password</label>-->
            <!--                        <input type="password"-->
            <!--                               class="form-control -->
            <?php //echo $model->hasError('password') ? 'is-invalid' : ''; ?><!--"-->
            <!--                               id="password" name="password"/>-->
            <!--                        <div class="invalid-feedback">-->
            <!--                            --><?php //echo $model->getError('password'); ?>
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                    <div class="mb-3">-->
            <!--                        <label class="form-label">Confirm Password</label>-->
            <!--                        <input type="password"-->
            <!--                               class="form-control -->
            <?php //echo $model->hasError('confirmPassword') ? 'is-invalid' : ''; ?><!--"-->
            <!--                               id="confirm-password" name="confirmPassword"/>-->
            <!--                        <div class="invalid-feedback">-->
            <!--                            --><?php //echo $model->getError('confirmPassword'); ?>
            <!--                        </div>-->
            <!--                    </div>-->
            <!--                    <button type="submit" class="btn btn-primary">Submit</button>-->
            <!--                </div>-->
            <!---->
            <!--            </form>-->
            <!--        -->

        </div>
    </div>
</div>
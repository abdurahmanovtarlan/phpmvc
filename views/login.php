<?php
/**
 * @var $model \app\models\User
 * @var $this \app\core\View;
 */


$this->title = 'Login';
?>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <?php $form = \app\core\form\Form::begin(['url' => "/login", 'method' => 'post']); ?>
            <div class="card my-5 shadow-sm">
                <div class="card-body">
                    <h5>Login</h5>
                    <?php echo $form->field($model, 'email'); ?>
                    <?php echo $form->field($model, 'password')->password(); ?>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary mt-2 w-100">Submit</button>
                    </div>
                </div>
            </div>
            <?php \app\core\form\Form::end(); ?>
        </div>
    </div>
</div>
<?php


require_once __DIR__ . '/../vendor/autoload.php';

use app\controllers\PostController;
use \app\core\Application;
use \app\controllers\SiteController;
use \app\controllers\AuthController;

$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$config = [
    'userClass' => \app\models\User::class,
    'database' => [
        'driver' => $_ENV['DB_CONNECTION'],
        'host' => $_ENV['DB_HOST'],
        'post' => $_ENV['DB_PORT'],
        'database' => $_ENV['DB_DATABASE'],
        'username' => $_ENV['DB_USERNAME'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

$app = new Application(dirname(__DIR__), $config);


$app->router->get('/', [SiteController::class, 'home']);
$app->router->get('/contact', [SiteController::class, 'contact']);
$app->router->post('/contact', [SiteController::class, 'contact']);

$app->router->get('/posts', [PostController::class, 'index']);
$app->router->get('/post/create', [PostController::class, 'post']);
$app->router->get("/post/update/:id", [PostController::class, 'update']);
$app->router->post("/post/create", [PostController::class, 'post']);
$app->router->post("/post/update/:id", [PostController::class, 'updatePost']);
$app->router->post("/post/delete", [PostController::class, 'delete']);

$app->router->get('/login', [AuthController::class, 'login']);
$app->router->post('/login', [AuthController::class, 'login']);
$app->router->get('/register', [AuthController::class, 'register']);
$app->router->post('/register', [AuthController::class, 'register']);

$app->router->get('/logout', [AuthController::class, 'logout']);
$app->router->get('/profile', [AuthController::class, 'profile']);


// Api ucun
//$app->router->get('/api/users', [AuthController::class, 'users']);
//$app->router->post('/api/user/add', [AuthController::class, 'userAdd']);
//$app->router->post('/api/user/delete', [AuthController::class, 'userDelete']);




$app->run();

